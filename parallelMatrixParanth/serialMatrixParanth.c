#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <stdio.h>

#define N 30 // size of matrix chain
FILE *file;

int map(int i, int j){
    return (i-1)*N + (j - 1);
}

int* readFile(int p[N+1]){
    int n = 0;
    FILE *fp;

    fp = fopen("input.txt", "r");
    /* fopen returns 0, the NULL pointer, on failure */
    if ( fp == 0 )
    {
        printf( "Could not open file\n" );
    }
    else {

        int x;
        int i = 10;
        int j;

        /* read one character at a time from file, stopping at EOF, which
           indicates the end of the file. */
        while ((x = fgetc(fp)) != EOF) {
            j = 0;
            if (x == '=' && n == 0) {
                x = fgetc(fp);
                while (x != 'x') {
                    j = j * i + (x - 48);
                    x = fgetc(fp);
                }
                p[n++] = j;
                j = 0;
            }
            if (x == 'x'){
                x = fgetc(fp);
                while (x != '\n') {
                    j = j * i + (x-48);
                    x = fgetc(fp);
                }
                p[n++] = j;

            }
        }
    }
    return p;
}

//storing the data into file recursively
//Note: Some times the program endup with segment fault because of this recursion. Seems that it doesn't likes recursion.
void saveAnswer(int *S, int i, int j) {
    if(i==j) {
        fprintf(file, "A%d", i);
    }
    else {
        fprintf(file, "(");
        saveAnswer(S,i,S[map(i, j)]);
        saveAnswer(S,S[map(i, j)]+1,j);
        fprintf(file, ")");
    }
}


// minimum cost function for matrix chain multiplication
int optimalCost(int i, int j,int *p,long long int *C, int *S) {
    int temp, result=0;
    int k=i;
    S[map(i, j)] = k;
    result = C[map(i, k)] + C[map(k+1, j)] + p[i-1]*p[k]*p[j];
    for(k=i+1; k != j; ++k ){
        temp = C[map(i, k)] + C[map(k+1, j)] + p[i-1]*p[k]*p[j];
        if (temp < result) {
            result = temp;
            S[map(i, j)] = k;
        }
    }
    return result;
}

// MAIN
int main(int argc, char *argv[]) {

    int p [128+1];
    readFile(p);

    long long int C [N*N] = {0};
    int S [N*N] = {0};


    int i, j, k;

        //filling the two tables C and S by going diagonal by diagonal where each processes is responsible for its own diagonal
        for (k = 1; k != N; ++k) {
            for (i = 1; i != N; ++i) {
                j = i + k;
                if (j <= N) {
                    C[map(i, j)] = optimalCost(i, j, p, C, S);
                }
            }
        }
        file = fopen("output.txt", "w");
        if ( file == 0 ) {
            printf( "Could not open file\n" );
        }else
            saveAnswer(S, 1, N);

        fclose(file);

    return 0;

}