#include "mpi.h"
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <stdio.h>

#define N 30 // size of matrix chain
FILE *file;

//auxiliary function used to map 1-dimensional array into 2-dimensional array
int map(int i, int j){
    return (i-1)*N + (j - 1);
}

//reading the input file for matrix sizes and storing them into array of size N+1
void readFile(int p[N+1]){
    int n = 0;
    FILE *fp;

    fp = fopen("input.txt", "r");
    /* fopen returns 0, the NULL pointer, on failure */
    if ( fp == 0 )
    {
        printf( "Could not open file\n" );
    }
    else {

        int x;
        int i = 10;
        int j;

        /* read one character at a time from file, stopping at EOF, which
           indicates the end of the file. */
        while ((x = fgetc(fp)) != EOF) {
            j = 0;
            if (x == '=' && n == 0) {
                x = fgetc(fp);
                while (x != 'x') {
                    j = j * i + (x - 48);
                    x = fgetc(fp);
                }
                p[n++] = j;
                j = 0;
            }
            if (x == 'x'){
                x = fgetc(fp);
                while (x != '\n') {
                    j = j * i + (x-48);
                    x = fgetc(fp);
                }
                p[n++] = j;

            }
        }
    }
}

//storing the data into file recursively
//Note: Some times the program endup with segment fault because of this recursion. Seems that it doesn't likes recursion.
void saveAnswer(int *S, int i, int j) {
    if(i==j) {
        fprintf(file, "A%d", i);
    }
    else {
        fprintf(file, "(");
        saveAnswer(S,i,S[map(i, j)]);
        saveAnswer(S,S[map(i, j)]+1,j);
        fprintf(file, ")");
    }
}


// minimum cost function for matrix chain multiplication
int optimalCost(int i, int j,int *p,int *C, int *S) {
    int temp, result=0;
    int k=i;
    S[map(i, j)] = k;
    result = C[map(i, k)] + C[map(k+1, j)] + p[i-1]*p[k]*p[j];
    for(k=i+1; k != j; ++k ){
        temp = C[map(i, k)] + C[map(k+1, j)] + p[i-1]*p[k]*p[j];
        if (temp < result) {
            result = temp;
            S[map(i, j)] = k;
        }
    }
    return result;
}

// MAIN
int main(int argc, char *argv[]) {

    int number_of_processes, subworld_size; // number of tasks
    int task_id, myId; // rank

    double startTime, endTime; // time stamps

    MPI_Init(&argc, &argv);

    // get number of tasks and current id
    MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
    MPI_Comm_rank(MPI_COMM_WORLD, &myId);
    MPI_Status Status;

    MPI_Comm subMatrixWorld;

    //declaring array for matrix sizes
    int p [128+1];
    readFile(p);

    MPI_Barrier(MPI_COMM_WORLD);
    //initializing C and S tables to store the results of each dynamic iteration
    //buffer arrays are used as temporary arrays for passing them between processes
    int C [N*N] = {0};
    long int bufferC[N*N] = {0};
    int S [N*N] = {0};
    int bufferS [N*N] = {0};


    int i, j, k, m, l;

   // record start time
    if (task_id == 0)   startTime = MPI_Wtime();

    //allow maximum number of processes equal to number of matrices
    if (myId < N) {
        //calculating the real number of participating processes
        if (number_of_processes >= N) {
            subworld_size = N;
            MPI_Comm_split(MPI_COMM_WORLD, subworld_size, myId, &subMatrixWorld);
            MPI_Comm_rank(subMatrixWorld, &task_id);
        } else {
            subworld_size = number_of_processes;
            MPI_Comm_split(MPI_COMM_WORLD, subworld_size, myId, &subMatrixWorld);
            MPI_Comm_rank(subMatrixWorld, &task_id);
        }

        //filling the two tables C and S by going diagonal by diagonal where each processes is responsible for its own diagonal
        //going in bottom-up fashion
        for (k = 1; k != N; ++k) {
            for (i = 1; i != N; ++i) {
                j = i + k;
                if (j % subworld_size == task_id && j <= N) {
                    C[map(i, j)] = optimalCost(i, j, p, C, S);
                    bufferC[map(i, j)] = C[map(i, j)];
                    bufferS[map(i, j)] = S[map(i, j)];

                    //transmitting buffers between all involved processes
                    for(l = 0; l != subworld_size; ++l){
                        if(l != task_id) {
                            MPI_Send(bufferC, N * N, MPI_INT, l, 0, subMatrixWorld);
                            MPI_Send(bufferS, N * N, MPI_INT, l, 0, subMatrixWorld);
                        }
                    }
                }
                //receiving the data from other processes
                if(j % subworld_size != task_id && j<= N){
                    MPI_Recv(bufferC, N*N, MPI_INT, j%subworld_size, 0, subMatrixWorld, &Status);
                    MPI_Recv(bufferS, N*N, MPI_INT, j%subworld_size, 0, subMatrixWorld, &Status);

                    //storing the data from buffers into main tables
                    for (m = 0; m != N * N; ++m) {
                        if (C[m] == 0 && bufferC[m] != 0) {
                            C[m] = bufferC[m];
                            S[m] = bufferS[m];
                        }
                    }

                }
            }
        }
    }

    // wait until all tasks complete
    MPI_Barrier(MPI_COMM_WORLD);


    if (task_id == 0) {
        endTime = MPI_Wtime( );
        printf("Total time: %.0f s\n",endTime - startTime);
        file = fopen("output.txt", "w");
        if ( file == 0 )
        {
            printf( "Could not open file\n" );
        }else
            //constructing the optimal solution from S table
            saveAnswer(S, 1, N);

        fclose(file);
    }

    MPI_Finalize();

    return 0;

}