The program runs as ussual, but there are some requirements towards input file as the numbers shouldn't be to high for the matrix size, as all data in C table is stored as simple integers. So, for large numbers there is a high risk of integer overflow to occur. 

Also, some times the print to output file fails as the algorithm is based on recursion and I assume that for large tables the recursion stack doesn't hold. 
