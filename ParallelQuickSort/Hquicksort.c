#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
//#include <fcntl.h>
//#include <sys/stat.h>
#include <math.h>


//merging two arrays
void merge(int ownedHalfData[ ], //own data
        int receivedHalfData[ ], //half received
        int data[ ], //data store
        int ownedDataLength, // first length
        int receivedDataLength) //second length
{
    int     i,      /* Loop control for array a */
            j,      /* Loop control for array b */
            k;      /* Loop control for array c */

            i = 0;
            j = 0;
            k = 0;


    while ((i < ownedDataLength) && (j < receivedDataLength)){
        if (ownedHalfData[i] < receivedHalfData[j]){
            data[k] = ownedHalfData[i];
            k++;
            i++;
        }else{
            data[k] = receivedHalfData[j];
            k++;
            j++;
        };
    }


    /* Pick up any remainder */
    while (i < ownedDataLength){
        data[k] = ownedHalfData[i];
        k++;
        i++;
    };
    while (j < receivedDataLength){
        data[k] = receivedHalfData[j];
        k++;
        j++;
    };
}

//swap function used for quicksort
void Swap(int *i, int *j) {
    int temp;	

    temp = *i;
    *i = *j;
    *j = temp;
}


//Median function: that returns the pivot

void Median(int a[], int left, int right, int *pivot) {
    int center;   	// for determining median 

    center = (left + right) / 2;

    if (a[left] > a[center]){
        Swap (&a[left], &a[center]);
    }
    if (a[left] > a[right]){
        Swap (&a[left], &a[right]);
    }
    if (a[center] > a[right]){
        Swap (&a[center], &a[right]);
    }

    *pivot = a[center];
    Swap (&a[center], &a[right - 1]);
}

//implimenting sequential quicksort
void QuickSort(int a[], int left, int right) {
    int
            i, 		
            j, 		
            pivot;		

    if (left < right) {
        Median(a, left, right, &pivot); 
        
        i = left;
        j = right - 1;
        if (i < j) {
            do {
                do {
                    i++;
                } while (a[i] < pivot);
                do {
                    j-- ;
                } while (a[j] > pivot);
                Swap(&a[i], &a[j]);
            } while (j > i);

            Swap(&a[i], &a[j]);
            Swap(&a[i], &a[right - 1]);  	//restoring the pivot
        };

        QuickSort(a, left, i-1);
        QuickSort(a, i+1, right);
    }
}



int main( int argc, char *argv[ ] ) {

    int *data;        //array for storing spawned main array
    int *array;
    int *lower;
    int *neighborData;
    int *higher;

    int
            remainder,            //amount of  unsplitted numbers
            length;              // length of splitted arrays



    int number_of_processes, task_id, dimension, numberOfNodes,
            numberOfCubes, cubeSize, myCubeNumber, localMedian, myId, pivot,
            lowerLength, upperLength, neighbor, halfLength, array_size, size;


    MPI_Status Status;
    double startTime, endTime;
    FILE *file = NULL;
    int i, j, k;
    MPI_Comm subCubeWorld;    // The following variable is needed to do subgroup communication


    MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
    MPI_Comm_rank(MPI_COMM_WORLD, &task_id);

    dimension = floor(log2((double) number_of_processes));
    numberOfNodes = 1 << dimension;
    array_size = atoi(argv[3]);
    length = array_size / numberOfNodes;
    remainder = array_size - length * numberOfNodes;

    int medians[length+1];

    if (task_id == 0) {
        //reading the numbers from the input.txt file
        file = fopen(argv[1], "r");
        array = (int *) malloc(array_size * sizeof(int));

        for (i = 0; i < array_size; i++) {
            fscanf(file, "%d", &(array[i]));
        }

        fclose(file);

        //splitting the data between processes
        for (i = 1; i < numberOfNodes; i++) {

            if (i >= remainder) {
                MPI_Send(&array[length * i + remainder], length, MPI_INT, i, 0, MPI_COMM_WORLD);
            } else {
                MPI_Send(&array[length * i + (i < remainder) * i], length+1, MPI_INT, i, 0, MPI_COMM_WORLD);
            }
        }

        /* Length for root */
        if (remainder)
            length = length+1;

        data = (int *) malloc(length * sizeof(int));

        for (i = 0; i != length; ++i) {
            data[i] = array[i];
        }

    } else if (task_id < numberOfNodes) {
        if (task_id >= remainder) {
            data = (int *) malloc(length * sizeof(int));
            MPI_Recv(data, length, MPI_INT, 0, 0, MPI_COMM_WORLD, &Status);
            MPI_Get_count(&Status, MPI_INT, &length);

        } else {
            data = (int *) malloc((length + 1) * sizeof(int));
            MPI_Recv(data, length + 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &Status);
            MPI_Get_count(&Status, MPI_INT, &length);
        }
    }

    MPI_Barrier(MPI_COMM_WORLD);

    startTime = MPI_Wtime();

    /* Do only if this processor is participating */
    if (task_id < numberOfNodes) {
        QuickSort(data, 0, length - 1);
    }

    //entering in the loop for d-dimensional hypercube
    for (i = dimension; i > 0; i--) {

        if (task_id < numberOfNodes) {

            //Determine the subcube
            numberOfCubes = 1 << (dimension - i);
            cubeSize = numberOfNodes / numberOfCubes;
            myCubeNumber = task_id >> i;

            //calculating the median for better load balancing
            if (length % 2) {
                localMedian = data[length / 2];
            } else {
                localMedian = (data[(length / 2) - 1]
                        + data[length / 2]) / 2;
            };
        }

        /* Setting up the subcube communication*/
        if (task_id < numberOfNodes) {

            MPI_Comm_split(MPI_COMM_WORLD, myCubeNumber, task_id, &subCubeWorld);
            MPI_Comm_rank(subCubeWorld, &myId);

        }

        if (task_id < numberOfNodes) {

            medians[myId] = localMedian;
        //broadcasting median to each processors
            for (j = 0; j < cubeSize; j++) {
                MPI_Bcast(&medians[j], 1, MPI_INT, j, subCubeWorld);
            }

            //sorting the array of broadcasted medians
            QuickSort(medians, 0, cubeSize - 1);

            //taken the median of medians as a pivot
            pivot = (medians[(cubeSize / 2) - 1]
                    + medians[cubeSize / 2]) / 2;

        //finding the lower and higher numbers according to the pivot
            j = 0;
            lowerLength = 0;
            upperLength = 0;

            while (j < length) {
                if (data[j] <= pivot) {
                    lowerLength++;
                } else {
                    upperLength++;
                }
                j++;
            }

            lower = (int *) malloc(lowerLength * sizeof(int));
            higher = (int *) malloc(upperLength * sizeof(int));

            j = 0;
            k = 0;

            while ((data[j] <= pivot) && (j < length)) {
                lower[k] = data[j];
                k++;
                j++;
            };

            k = 0;
            while ((data[j] > pivot) && (j < length)) {
                higher[k] = data[j];
                k++;
                j++;
            };

            //interchanging the lower and higher bounds with the neighbor

            neighbor = (task_id ^ (~(~0 << 1) << (i - 1)));

            size = 0;

            //first receiving the size of interchanging arrays
            if (task_id > neighbor) {
                MPI_Send(&lowerLength, 1, MPI_INT, neighbor, 0,
                        MPI_COMM_WORLD);

                MPI_Recv(&size, 1, MPI_INT, neighbor, 0,
                        MPI_COMM_WORLD, &Status);
            } else {
                MPI_Recv(&size, 1, MPI_INT, neighbor, 0,
                        MPI_COMM_WORLD, &Status);
                MPI_Send(&upperLength, 1, MPI_INT, neighbor, 0,
                        MPI_COMM_WORLD);
            };

            neighborData = (int *) malloc(size * sizeof(int));

            if (task_id > neighbor) {

                // Send lower part to neighbor
                MPI_Send(lower, lowerLength, MPI_INT, neighbor, 0,
                        MPI_COMM_WORLD);


                // Receive higher part from neighbor
                MPI_Recv(neighborData, size, MPI_INT, neighbor, 0,
                        MPI_COMM_WORLD, &Status);
                MPI_Get_count(&Status, MPI_INT, &halfLength);

            } else {

                // Receive lower part from neighbor
                MPI_Recv(neighborData, size, MPI_INT, neighbor, 0,
                        MPI_COMM_WORLD, &Status);
                MPI_Get_count(&Status, MPI_INT, &halfLength);

                // Send higher part to neighbor
                MPI_Send(higher, upperLength, MPI_INT, neighbor, 0,
                        MPI_COMM_WORLD);
            };

            // Merging owned lower/higher array with the new received array
            if (task_id > neighbor) {
                free(data);
                length = upperLength + halfLength;
                data = (int *) malloc(length * sizeof(int));

                merge(higher, neighborData, data, upperLength, halfLength);
                free(neighborData);
                free(lower);
                free(higher);


            } else {
                free(data);
                length = lowerLength + halfLength;
                data = (int *) malloc(length * sizeof(int));
                merge(lower, neighborData, data, lowerLength, halfLength);
                free(neighborData);
                free(lower);
                free(higher);

            };
        };
    };

    //storing the data from root processor into main array
    if(task_id == 0){
        free(array);
        array = (int *) malloc(array_size * sizeof(int));
        for(i=0;i!=length;++i){
            array[i] = data [i];
        }
        size = length;
    }

    //sending all the rest of sorted arrays to the root and storing them into main array
    for(i=1;i!=number_of_processes;++i){
        if(task_id == i){

            MPI_Send(&length, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
            MPI_Send(data, length, MPI_INT, 0, 0, MPI_COMM_WORLD);
        }
        if(task_id == 0){
            MPI_Recv(&length, 1, MPI_INT, i, 0, MPI_COMM_WORLD, &Status);
            MPI_Recv(&array[size], length, MPI_INT, i, 0, MPI_COMM_WORLD, &Status);
            size = size + length;
        }
    }

    MPI_Barrier( MPI_COMM_WORLD );
    endTime = MPI_Wtime( ) - startTime;

    //storing into output.txt file
    if (task_id == 0) {
        file = fopen(argv[2], "w");

        for (i = 0; i < size; i++)
           // printf("%d, ", array[i]);
            fprintf(file, "%d ", array[i]);
        fclose(file);

        printf("Array size: %d, Time: %f\n", array_size, endTime);
    }

    MPI_Finalize( );
    return (0);

}


