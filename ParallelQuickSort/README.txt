running command:
mpirun -np (# of processes) Hquicksort input.txt output.txt 1000

the first argument is the input file
the second is the name of output file
and the last is the number of problem size (array size)

you'll find 3 types of input files with 1k, 10k and 100k array size.
(input.txt, input10k.txt and input100k.txt respectively).
