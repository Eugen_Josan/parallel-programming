#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

//swap function used for quicksort
void Swap(int *i, int *j) {
    int temp;

    temp = *i;
    *i = *j;
    *j = temp;
}


//Pivot function: that returns the pivot

void Pivot(int a[], int left, int right, int *pivot) {
    int center;   	// for determining the pivot

    center = (left + right) / 2;

    if (a[left] > a[center]){
        Swap (&a[left], &a[center]);
    }
    if (a[left] > a[right]){
        Swap (&a[left], &a[right]);
    }
    if (a[center] > a[right]){
        Swap (&a[center], &a[right]);
    }

    *pivot = a[center];
    Swap (&a[center], &a[right - 1]);
}

//implimenting sequential quicksort
void QuickSort(int a[], int left, int right) {
    int
            i,
            j,
            pivot;

    if (left < right) {
        Pivot(a, left, right, &pivot);

        i = left;
        j = right - 1;
        if (i < j) {
            do {
                do {
                    i++;
                } while (a[i] < pivot);
                do {
                    j-- ;
                } while (a[j] > pivot);
                Swap(&a[i], &a[j]);
            } while (j > i);

            Swap(&a[i], &a[j]);
            Swap(&a[i], &a[right - 1]);  	//restoring the pivot
        };

        QuickSort(a, left, i-1);
        QuickSort(a, i+1, right);
    }
}
int main( int argc, char *argv[ ] ) {

    FILE *file = NULL;
    int *array;
    int array_size, i;
    double startTime, endTime;



    array_size = atoi(argv[3]);

    MPI_Init(&argc, &argv);

    file = fopen(argv[1], "r");
    array = (int *) malloc(array_size * sizeof(int));

    for (i = 0; i < array_size; i++) {
        fscanf(file, "%d", &(array[i]));
    }

    fclose(file);


    startTime = MPI_Wtime();
    QuickSort(array, 0, array_size - 1);
    endTime = MPI_Wtime( ) - startTime;

    file = fopen(argv[2], "w");

    for (i = 0; i < array_size; i++)
        // printf("%d, ", array[i]);
        fprintf(file, "%d ", array[i]);
    fclose(file);

    printf("Array size: %d, Time: %f\n", array_size, endTime);

    MPI_Finalize( );
    return (0);

}